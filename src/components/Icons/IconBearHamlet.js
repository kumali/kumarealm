import React from 'react';
import PropTypes from 'prop-types';

const IconBearHemlet = props => (
    <svg xmlns="http://www.w3.org/2000/svg" width={props.size} height={props.size*1.179} viewBox="0 0 464 547">
        <g fill="none" fill-rule="evenodd" stroke="#979797" transform="translate(0 -33)">
            <g transform="translate(7 126)">
                <g transform="translate(2)">
                    <circle cx="64" cy="64" r="61" fill="#FFF" stroke-width="6"/>
                    <circle cx="65" cy="65" r="29" fill="#979797" stroke-width="6"/>
                    <g transform="translate(322)">
                        <circle cx="64" cy="64" r="61" fill="#FFF" stroke-width="6"/>
                        <circle cx="65" cy="65" r="29" fill="#979797" stroke-width="6"/>
                    </g>
                </g>
                <circle cx="227" cy="227" r="224" fill="#FFF" stroke-width="6"/>
                <g fill="#777" transform="translate(130 169)">
                    <circle cx="16" cy="16" r="13" stroke-width="6"/>
                    <circle cx="16" cy="16" r="13" stroke-width="6" transform="translate(162)"/>
                </g>
                <g transform="translate(142 238)">
                    <ellipse cx="85.5" cy="60.5" fill="#D8D8D8" stroke-width="6" rx="82.5" ry="57.5"/>
                    <ellipse cx="28" cy="16.5" fill="#A3A3A3" stroke-width="6" rx="25" ry="13.5" transform="translate(57 21)"/>
                    <g fill="#D8D8D8" transform="translate(69 54)">
                    <rect width="1" height="18" x="10" y="31" stroke-width="6" rx=".5" transform="scale(-1 1) rotate(-37 0 69.887)"/>
                    <rect width="1" height="26" x="17" y="3" stroke-width="6" rx=".5" transform="matrix(-1 0 0 1 34 0)"/>
                    <rect width="1" height="10" x="21" y="31" stroke-width="6" rx=".5" transform="scale(-1 1) rotate(37 0 -26.762)"/>
                    </g>
                </g>
            </g>
            <g fill="#FFF" stroke-width="6" transform="translate(0 33)">
                <path d="M441.965575,210 C439.908637,120.434561 345.932211,48 230,48 C114.067789,48 20.091363,120.434561 18.0344246,210 L441.965575,210 Z"/>
                <rect width="458" height="42" x="3" y="205" rx="8"/>
                <g transform="translate(172)">
                    <rect width="66" height="202" x="27" y="3" rx="8"/>
                    <ellipse cx="61" cy="72.749" rx="47" ry="48.232"/>
                    <path d="M52.0140537,35.6509779 C38.0203329,38.8125353 26.9862732,50.108773 23.8791234,64.4788924 L52.0140606,35.6509764 Z"/>
                </g>
            </g>
        </g>
    </svg>
);

IconBearHemlet.propTypes = {
    size: PropTypes.number
};

IconBearHemlet.defaultProps = {
    size: 464
};

export default IconBearHemlet;